# README

1. Добавить ip адрес
```rdoc
curl -X POST \
  http://snsll.kiwibird.ru/api/ip_addresses/v1/add \
  -H 'Content-Type: application/json' \
  -d '{"ip_address": "8.8.8.8"}'
```
2. Убрать ip адрес

```rdoc
curl -X POST \
  http://snsll.kiwibird.ru/api/ip_addresses/v1/remove \
  -H 'Content-Type: application/json' \
  -d '{"ip_address": "8.8.8.8"}'
```

3. Посмотреть статистику по адресу
```rdoc
curl -X GET \
  'http://snsll.kiwibird.ru/api/statistic/v1/?ip_address=8.8.8.8&from=20190624&till=20190625'
```
