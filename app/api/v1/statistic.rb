class Statistic < Grape::API
  format :json
  rescue_from ActiveRecord::StatementInvalid do |_e|
    error! 'Ip address is invalid', 422
  end

  params do
    requires :from, type: DateTime, coerce_with: DateTime.method(:iso8601)
    requires :till, type: DateTime, coerce_with: DateTime.method(:iso8601)
    requires :ip_address, type: String
  end

  get('/') do
    ip = IpAddress.includes(:ping_requests).find_by(address: params[:ip_address])
    pings = ip.ping_requests.where(created_at: params[:from]..params[:till])
    if pings.where(success: true).present?
      StatisticDecorator.decorate(pings).data
    else
      error!('No success ping requests in this time', 422)
    end
  end
end
