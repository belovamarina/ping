class Root < Grape::API
  version 'v1'
  format :json

  mount IpAddresses => 'ip_addresses'
  mount Statistic => 'statistic'
end
