class IpAddresses < Grape::API
  format :json
  rescue_from ActiveRecord::StatementInvalid do |_e|
    error! 'Ip address is invalid', 422
  end

  params do
    requires :ip_address, type: String, desc: 'ip address to add'
  end

  post('/add') do
    service = IpAddressCreator.new(params[:ip_address])
    if service.call
      { status: :ok }
    else
      error!('Ip address is invalid', 422)
    end
  end

  params do
    requires :ip_address, type: String, desc: 'ip address to remove'
  end

  post('/remove') do
    if (ip = IpAddress.find_by(address: params[:ip_address]))
      ip.update(active: false)
      { status: :ok }
    else
      error!('Not found', 404)
    end
  end
end
