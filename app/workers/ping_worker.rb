class PingWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(ip_id)
    ip = IpAddress.find_by(id: ip_id)
    return unless ip&.active?

    result = []
    service = Ping::Icmp.new(host: ip.address.to_s)
    3.times { result << service.ping }
    ip.ping_requests.create(result)
  end
end
