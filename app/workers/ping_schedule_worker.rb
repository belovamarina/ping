class PingScheduleWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform
    active_ips = IpAddress.where(active: true)

    active_ips.find_each do |ip|
      PingWorker.perform_async(ip.id)
    end
  end
end
