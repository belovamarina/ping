require 'resolv'

class IpAddressCreator
  def initialize(address)
    @address = address
  end

  def call
    return unless valid?

    create_or_update_ip_address
  end

  private

  attr_reader :address

  def valid?
    address.present? && address.match?(Regexp.union(::Resolv::IPv4::Regex, ::Resolv::IPv6::Regex))
  end

  def create_or_update_ip_address
    ip = IpAddress.find_by(address: address)
    ip ? ip.update(active: true) : IpAddress.create(address: address, active: true)
  end
end
