module Ping
  # Inspired with net-ping gem: https://github.com/chernesk/net-ping
  class Icmp
    ICMP_ECHOREPLY = 0 # Echo reply
    ICMP_ECHO      = 8 # Echo request
    ICMP_SUBCODE   = 0
    SOCKET_ATTRS = {
      macos: [Socket::AF_INET, Socket::SOCK_DGRAM, Socket::IPPROTO_ICMP], # non-privileged http://www.manpagez.com/man/4/icmp/
      linux: [Socket::PF_INET, Socket::SOCK_RAW, Socket::IPPROTO_ICMP] # need setcap cap_net_raw+ep path/to/bin or sudo
    }.freeze

    attr_reader :host, :data_size, :seq, :timeout, :logger

    def initialize(host:, data_size: 56, timeout: 10, logger: Rails.logger)
      @host = host
      @data_size = data_size
      @timeout = timeout
      @seq = 0
      @logger = logger
    end

    def ping
      increase_sequence
      start_time = Time.now

      begin
        socket = Socket.new(*socket_attributes)
        socket.send(header, 0, socket_address)

        Timeout.timeout(timeout) do
          io_array = select([socket], nil, nil, timeout)
          return result(false, start_time) if io_array.nil? || io_array[0].empty?

          result(parse_response(socket), start_time)
        end
      rescue StandardError => e
        logger.error "#{e.class} #{e.message}\n#{e.backtrace.join("\n")}"
        result(false, start_time)
      ensure
        socket&.close
      end
    end

    private

    def socket_attributes
      SOCKET_ATTRS[ENV.fetch('OP_SYSTEM', 'linux').to_sym]
    end

    def increase_sequence
      @seq = (@seq + 1) % 65_536
    end

    def socket_address
      Socket.pack_sockaddr_in(0, host)
    end

    def header
      checksum = 0
      msg = [ICMP_ECHO, ICMP_SUBCODE, checksum, ping_id, seq, data].pack(pstring)

      checksum = checksum(msg)
      [ICMP_ECHO, ICMP_SUBCODE, checksum, ping_id, seq, data].pack(pstring)
    end

    # https://tools.ietf.org/html/rfc1071
    def checksum(msg)
      length    = msg.length
      num_short = length / 2
      check     = 0

      msg.unpack("n#{num_short}").each do |short|
        check += short
      end

      check += msg[length - 1, 1].unpack1('C') << 8 if (length % 2).positive?

      check = (check >> 16) + (check & 0xffff)
      (~((check >> 16) + check) & 0xffff)
    end

    def data
      @data ||= (0..data_size).inject('') { |memo, n| memo << (n % 256).chr }
    end

    def ping_id
      @ping_id ||= (Thread.current.object_id ^ Process.pid) & 0xffff
    end

    def pstring
      'C2 n3 A' << data_size.to_s
    end

    def parse_response(socket)
      loop do
        data = socket.recvfrom(1500).first
        type = data[20, 2].unpack1('C2')

        case type
        when ICMP_ECHOREPLY
          res_ping_id, res_seq = data[24, 4].unpack('n3')
        else
          res_ping_id, res_seq = data[52, 4].unpack('n3')
        end

        break true if res_ping_id == @ping_id && res_seq == @seq && type == ICMP_ECHOREPLY
      end
    end

    def result(success, start_time)
      logger.info "ping_id: #{ping_id} seq: #{seq}, #{host} #{success}"
      duration = Time.now - start_time if success
      { seq: seq, success: success, started_at: start_time, duration: duration }
    end
  end
end
