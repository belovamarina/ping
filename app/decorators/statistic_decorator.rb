class StatisticDecorator < Draper::Decorator
  def min
    object.minimum(:duration)
  end

  def max
    object.maximum(:duration)
  end

  def average
    object.average(:duration).to_f
  end

  def sorted_scope
    @sorted_scope ||= object.order(:duration).pluck(:duration).compact
  end

  def count
    @count ||= object.count
  end

  def sorted_count
    sorted_scope.size
  end

  def packet_loss_percent
    (object.where(success: false).count * 100.0) / count.to_f
  end

  def median
    (sorted_scope[(sorted_count - 1) / 2] + sorted_scope[sorted_count / 2]) / 2.0
  end

  def standard_deviation
    avg = average
    sum = sorted_scope.inject(0) { |accum, i| accum + (i - avg)**2 }
    Math.sqrt(sum / (sorted_scope.count - 1).to_f)
  end

  def data
    {
      min_ttl: min,
      max_ttl: max,
      average: average,
      median: median,
      packet_loss: "#{packet_loss_percent.round(3)} %",
      standard_deviation: standard_deviation
    }
  end
end
