class IpAddress < ApplicationRecord
  validates :address, presence: true, uniqueness: true

  has_many :ping_requests
end
