FactoryBot.define do
  factory :ip_address do
    address '127.0.0.1'
    active true
  end
end
