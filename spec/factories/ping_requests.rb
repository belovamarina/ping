FactoryBot.define do
  factory :ping_request do
    ip_address
    success true
    duration { rand(0.001..0.999) }
  end
end
