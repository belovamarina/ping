require 'rails_helper'

describe IpAddresses do
  describe 'POST#add' do
    let(:ip) { IpAddress.last }

    subject { post '/api/ip_addresses/v1/add', params: { ip_address: address } }

    context 'when correct params' do
      let(:address) { '8.8.8.8' }

      it 'responses with success status' do
        subject
        expect(response.status).to eq(201)
      end

      it 'creates ip address' do
        expect { subject }.to change { IpAddress.count }.to(1)
      end

      it 'creates ip address with active status' do
        subject
        expect(ip.active).to eq(true)
        expect(ip.address.to_s).to eq(address)
      end
    end

    context 'when invalid params' do
      let(:address) { '12345678' }

      it 'responses with 422 status' do
        subject
        expect(response.status).to eq(422)
        expect(response.body).to eq('{"error":"Ip address is invalid"}')
      end

      it 'does not create ip address' do
        expect { subject }.not_to change { IpAddress.count }
      end
    end
  end

  describe 'POST#remove' do
    let(:ip) { create(:ip_address) }
    let(:address) { ip.address.to_s }

    subject { post '/api/ip_addresses/v1/remove', params: { ip_address: address } }

    context 'when correct params' do
      it 'responses with success status' do
        subject
        expect(response.status).to eq(201)
      end

      it 'updates ip status to inactive' do
        expect { subject }.to change { ip.reload.active }.to(false)
      end
    end

    context 'when invalid params' do
      let(:address) { '8.8.8.9' }

      it 'responses with 404 status' do
        subject
        expect(response.status).to eq(404)
      end
    end
  end
end
