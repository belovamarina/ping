require 'rails_helper'

describe Statistic do
  describe 'GET' do
    let(:address) { '8.8.8.8' }
    let(:ip) { create(:ip_address, address: address) }
    let!(:ping_requests) { create_list(:ping_request, 10, ip_address_id: ip.id, created_at: '2019-06-24') }
    let(:params) { { ip_address: address, from: '2019-06-23', till: '2019-06-24' } }

    subject do
      get '/api/statistic/v1/', params: params
    end

    context 'when correct params' do
      it 'responses with success status' do
        subject
        expect(JSON.parse(response.body)).to include('average', 'max_ttl', 'median', 'min_ttl',
                                                     'packet_loss', 'standard_deviation')
      end
    end

    context 'when invalid params' do
      let(:params) { { ip_address: address, from: '2019-06-25', till: '2019-06-26' } }

      it 'responses with 422 status' do
        subject
        expect(response.status).to eq(422)
        expect(response.body).to eq('{"error":"No ping requests in this time"}')
      end

      context 'when invalid ip' do
        let(:params) { { ip_address: '12345', from: '2019-06-25', till: '2019-06-26' } }

        it 'responses with 422 status' do
          subject
          expect(response.status).to eq(422)
          expect(response.body).to eq('{"error":"Ip address is invalid"}')
        end
      end
    end
  end
end
