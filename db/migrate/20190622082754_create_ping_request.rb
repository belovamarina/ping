class CreatePingRequest < ActiveRecord::Migration[5.1]
  def change
    create_table :ping_requests do |t|
      t.references :ip_address, null: false, index: true
      t.integer :seq
      t.datetime :started_at
      t.float :duration, index: true
      t.boolean :success
      t.timestamps
    end
  end
end
