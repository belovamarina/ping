class CreateIpAddress < ActiveRecord::Migration[5.1]
  def change
    create_table :ip_addresses do |t|
      t.inet :address, null: false
      t.boolean :active, null: false, default: true
      t.timestamps
    end
  end
end
